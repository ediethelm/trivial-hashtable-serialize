(uiop:define-package #:trivial-hashtable-serialize
  (:documentation "trivial-hashtable-serialize provides a simple and probably naïve way to serialize hash-tables.")
  (:use #:common-lisp)
  (:export #:load-hashtable
	   #:save-hashtable))

