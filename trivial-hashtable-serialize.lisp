;;;; Copyright (c) Eric Diethelm 2018 <ediethelm@yahoo.com>
;;;; This code is licensed under the MIT license.

(in-package :trivial-hashtable-serialize)

(defun default-serialize-fn (stream key value)
  "This is the default serialization function. It uses #\$ to separate key and values.

See also *DEFAULT-DESERIALIZE-FN*."
  (declare (type stream stream))
  (format stream "~a$\"~a\"~%" key  value))

(defun default-deserialize-fn (line)
  "This function deserializes key/value pairs serialized by *DEFAULT-SERIALIZE-FN*."
  (declare (type string line))
  (let* ((pair (split-sequence:split-sequence #\$ line))
	 (key-str (car pair))
	 (value-str (cadr pair)))
    (values (read-from-string key-str) (read-from-string value-str))))

(defun save-hashtable (table file-name &key (serialize-fn #'default-serialize-fn))
  "Serializes the hash-table *TABLE* into the file identified by *FILE-NAME*. A non-default serialization function can be given through *SERIALIZE-FN*.  
  
The signature of *SERIALIZE-FN* is (function (stream key value) null)."
  (declare (type function serialize-fn))

  (with-open-file (fstream file-name :direction :output :if-exists :supersede)
    (unless fstream
      (error "Could not open file '~a'" file-name))

    (loop for key being the hash-keys of table
       do (funcall serialize-fn fstream key (gethash key table)))))

(defun load-hashtable (file-name &key (if-does-not-exist :create) (deserialize-fn #'default-deserialize-fn))
  "Read the contents of *FILE-NAME* and deserialize it into a new hash-table.
*IF-DOES-NOT-EXIST* accepts the same values as the equaly named key in *WITH-OPEN-FILE*.
*DESERIALIZE-FN* can be defined to use a non-default serialization function.  
  
The signature of *DESERIALIZE-FN* is (function (line) (values key value))."
  (declare (type function deserialize-fn))

  (let ((table (make-hash-table)))
    (with-open-file (fstream file-name :direction :input :if-does-not-exist if-does-not-exist)
      (unless fstream
	(error "Could not open file '~a'" file-name))

      (loop for line = (read-line fstream nil 'eof)
	 until (eq line 'eof)
	 do
	   (multiple-value-bind (key value) (funcall deserialize-fn line)
	     (setf (gethash key table) value))))

    (return-from load-hashtable table)))

