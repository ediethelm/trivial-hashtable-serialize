;;;; -*- Mode: Lisp; Syntax: ANSI-Common-Lisp; Base: 10 -*-

(defsystem :trivial-hashtable-serialize
  :description "A simple method to serialize and deserialize hash-tables."
  :version "0.1.4"
  :author "Eric Diethelm <ediethelm@yahoo.com>"
  :licence "MIT"
  :depends-on (:split-sequence)
  :components ((:file "package")
	       (:file "trivial-hashtable-serialize")))

