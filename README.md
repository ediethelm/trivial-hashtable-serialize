# Trivial Hash-Table Serialization Manual

###### \[in package TRIVIAL-HASHTABLE-SERIALIZE\]
[![pipeline status](https://gitlab.com/ediethelm/trivial-hashtable-serialize/badges/master/pipeline.svg)](https://gitlab.com/ediethelm/trivial-hashtable-serialize/commits/master)
[![Quicklisp](http://quickdocs.org/badge/trivial-hashtable-serialize.svg)](http://quickdocs.org/trivial-hashtable-serialize/)

## Description

This simple library allows for simple serialization and deserialization of hash-tables.

## Installing trivial-hashtable-serialize

This project is available in the latest [QuickLisp](https://www.quicklisp.org/beta/ "QuickLisp") distribution, so installing it is reduced to calling:

```lisp
(ql:quickload :trivial-hashtable-serialize)
```

## Working Example

The code block below shows how to export/import a simple hashtable using the default serialization methods. Keep in mind, that this default does simply *(format)* the key and value. This makes serialization of keywords and strings erroneous.
To overcome this limitation, use a more elaborate (de)serialization function, as described further below.

```lisp
(let ((table (make-hash-table)))
  (setf (gethash 'a table) 9)
  (setf (gethash 'b table) 8)
  (setf (gethash 'c table) 7)
  (save-hashtable table "~/hashtable-export.txt"))

(let ((imported (load-hashtable "~/hashtable-export.txt")))
  (format t "Value of 'a is: ~a~%" (gethash 'a imported))
  (format t "Value of 'b is: ~a~%" (gethash 'b imported))
  (format t "Value of 'c is: ~a~%" (gethash 'c imported)))
```

As mentioned before, here a better implementation of the serialize function using trivial-json-codec.

```lisp
(defun json-serialize-fn (stream key value)
  (declare (type stream stream))
  (format stream "~a~%" (trivial-json-codec:serialize-json (list key value))))

(defun json-deserialize-fn (line)
  (let ((r (trivial-json-codec:deserialize-json line)))
    (values (car r) (cadr r))))

(let ((table (make-hash-table)))
  (setf (gethash "a" table) '(1 2 3))
  (setf (gethash "b" table) '(4 5 (7 8)))
  (setf (gethash "c" table) (make-array 4 :initial-contents '(7 6 5 4)))
  (save-hashtable table "~/hashtable-export-json.txt" :serialize-fn #'json-serialize-fn))

(let ((imported (load-hashtable "~/hashtable-export-json.txt" :deserialize-fn #'json-deserialize-fn)))
  (format t "Value of 'a' is: ~a~%" (gethash "a" imported))
  (format t "Value of 'b' is: ~a~%" (gethash "b" imported))
  (format t "Value of 'c' is: ~a~%" (gethash "c" imported)))
```

## Exported Symbols

- [function] SAVE-HASHTABLE TABLE FILE-NAME &KEY (SERIALIZE-FN #'DEFAULT-SERIALIZE-FN)

    Serializes the hash-table *TABLE* into the file identified by *FILE-NAME*. A non-default serialization function can be given through *SERIALIZE-FN*.  
    
    The signature of *SERIALIZE-FN* is (function (stream key value) null).

- [function] LOAD-HASHTABLE FILE-NAME &KEY (IF-DOES-NOT-EXIST :CREATE) (DESERIALIZE-FN #'DEFAULT-DESERIALIZE-FN)

    Read the contents of *FILE-NAME* and deserialize it into a new hash-table.
    *IF-DOES-NOT-EXIST* accepts the same values as the equaly named key in *WITH-OPEN-FILE*.
    *DESERIALIZE-FN* can be defined to use a non-default serialization function.  
    
    The signature of *DESERIALIZE-FN* is (function (line) (values key value)).

## License Information

This library is released under the MIT License. Please refer to the [LICENSE](https://gitlab.com/ediethelm/trivial-hashtable-serialize/blob/master/LICENSE "License") to get the full licensing text.

## Contributing to this project

Please refer to the [CONTRIBUTING](https://gitlab.com/ediethelm/trivial-hashtable-serialize/blob/master/CONTRIBUTING.md "Contributing") document for more information.

